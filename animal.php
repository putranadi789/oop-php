<?php
 class Animal {
	public $name;
	public $legs=4;
	public $cold_blooded="no";

	public function __construct($string){
		$this->name=$string;
	}
	function set_name($name){
		$this->name = $name;
	}
	function set_legs($legs){
		$this->legs = $legs;
	}
	function set_cold_blooded($cold_blooded){
		$this->cold_blooded = $cold_blooded;
	}
	function get_name(){
		return $this->name;
	}
	function get_legs(){
		return $this->legs;
	}
	function get_cold_blooded(){
		return $this->cold_blooded;
	}

}
?>
