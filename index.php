<?php
require ('Animal.php');
require ('Ape.php');
require ('Frog.php');

$sheep = new Animal("shaun");

echo "name = ".$sheep->get_name()."<br>";
echo "legs = ".$sheep->get_legs()."<br>"; 
echo "cold_blooded = ".$sheep->get_cold_blooded()."<br><br>"; 

$sungokong = new Ape("kera sakti");
$kodok = new Frog("buduk");

echo "name = ".$sungokong->get_name()."<br> legs = ".$sungokong->get_legs()."<br> cold bloded = ".$sungokong->get_cold_blooded()."<br> yell = ".$sungokong->yell()."<br><br>";
echo "name = ".$kodok->get_name()."<br> legs = ".$kodok->get_legs()."<br> cold bloded = ".$kodok->get_cold_blooded()."<br> Jump = ".$kodok->jump()."<br><br>";

?>
